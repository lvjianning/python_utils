import cv2
import requests
import base64
import time
import numpy as np

# 关键点颜色
pose_kpt_color = [
    [0, 255, 0], [0, 255, 0], [0, 255, 0], [0, 255, 0], [0, 255, 0], [255, 128, 0],
    [255, 128, 0], [255, 128, 0], [255, 128, 0], [255, 128, 0], [255, 128, 0], [51, 153, 255], [51, 153, 255],
    [51, 153, 255], [51, 153, 255], [51, 153, 255], [51, 153, 255]
]

# 骨架线颜色
pose_limb_color = [
    [51, 153, 255], [51, 153, 255], [51, 153, 255], [51, 153, 255], [255, 51, 255], [255, 51, 255],
    [255, 51, 255], [255, 128, 0], [255, 128, 0], [255, 128, 0], [255, 128, 0], [255, 128, 0], [0, 255, 0],
    [0, 255, 0], [0, 255, 0], [0, 255, 0], [0, 255, 0], [0, 255, 0], [0, 255, 0]
]

# 骨架线结构
skeleton = [[16, 14], [14, 12], [17, 15], [15, 13], [12, 13], [6, 12],
            [7, 13], [6, 7], [6, 8], [7, 9], [8, 10], [9, 11], [2, 3],
            [1, 2], [1, 3], [2, 4], [3, 5], [4, 6], [5, 7]]

# 绘制关键点
def draw_kpoints_and_limbs(kpts, image):
    height, width = image.shape[:2]

    # 绘制关键点
    for k in range(17):
        R, G, B = pose_kpt_color[k]
        x_coord = kpts[k*3+0]   
        y_coord = kpts[k*3 + 1]
        kpt_conf = kpts[k*3 + 2]
        if kpt_conf > 0.5:
            cv2.circle(image, (int(x_coord), int(y_coord)), 2, (R, G, B), -1)

    # 绘制骨架线
    for limb in skeleton:
        p1, p2 = limb[0] - 1, limb[1] - 1
        pos1 = (int(kpts[p1*3] ), int(kpts[p1*3 + 1] ))
        pos2 = (int(kpts[p2*3] ), int(kpts[p2*3 + 1]))

        conf1, conf2 = kpts[p1*3 + 2], kpts[p2*3 + 2]

        if conf1 > 0.5 and conf2 > 0.5:
            R, G, B = pose_limb_color[limb[0] - 1]
            cv2.line(image, pos1, pos2, (R, G, B), 2)

def visualImage(image, box_result):

    for ibox in box_result:
        kpts = ibox["kpts"]
        class_id = ibox['class_id']
        x=ibox['x'] 
        y=ibox['y']
        width=ibox['width']
        height=ibox['height']
        score =ibox['score']
        print(ibox)
        cv2.rectangle(image, (int(x), int(y)), (int(x + width), int(y + height)), (0, 0, 255), 2)
        caption = f"{class_id} {score:.2f}"
        cv2.putText(image, caption, (int(x), int(y) - 5), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), 2)
        draw_kpoints_and_limbs(kpts, image)

# 辅助函数：根据图像尺寸调整关键点坐标  
def scale_kpts(x, y, width, height):
    return x * width, y * height

def main():
    # 配置参数
    url = "rtsp://admin:abcd1234@192.168.1.112/smart265/ch1/sub/av_stream"
    # url=r"H:\测试视频\人\666.mp4"
    model_url = "http://192.168.1.66:10100/pose"
    
    # 视频捕获初始化
    cap = cv2.VideoCapture(url)
    if not cap.isOpened():
        print("Cannot open camera")
        return

    # 创建窗口
    cv2.namedWindow('IP Camera', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('IP Camera', 1280, 720)

    # 处理视频流
    while True:
        ret, frame = cap.read()
        
        if not ret:
            print("Failed to retrieve frame")
            break

        # 将图像帧编码为JPEG格式的二进制数据
        _, img_data = cv2.imencode('.jpg', frame)

        # 请求模型服务
        try:
            res = requests.post(model_url, data=img_data.tobytes())
            res = res.json()

            # 对于每个检测到的对象，绘制边界框和关键点
            if 'data' in res:
                visualImage(frame, res['data'])

        except requests.RequestException as e:
            print(f"Request to model service failed: {str(e)}")

        cv2.imshow('IP Camera', frame)

        # 按下ESC键退出程序
        if cv2.waitKey(1)==27:
            break

    # 释放资源
    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
