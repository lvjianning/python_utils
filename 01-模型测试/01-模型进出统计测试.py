import cv2
import requests
import time
import numpy as np
import threading
import queue
import concurrent.futures

# 初始化配置信息
class Config:
    def __init__(self):
        self.COUNT_DICT = {}
        self.count_enter = 0
        self.count_out = 0

# 计算点是否在直线的一侧
def bool_enter_out(point0, point1, point2):
    temp = (point1[1] - point2[1]) * point0[0] + (point2[0] - point1[0]) * point0[1] - point2[0] * point1[1] + point1[0] * point2[1]
    return temp

# 统计进出数量
def count_enter_out(t_id, temp, conf):
    if t_id not in conf.COUNT_DICT:
        conf.COUNT_DICT[t_id] = temp
    else:
        if conf.COUNT_DICT[t_id] < 0 and temp > 0:
            conf.count_out += 1
            conf.COUNT_DICT[t_id] = temp
        elif conf.COUNT_DICT[t_id] > 0 and temp < 0:
            conf.count_enter += 1
            conf.COUNT_DICT[t_id] = temp

# 结果处理
def plot_result(frame, results, conf):
    for res in results:
        
        class_id=""
        # class_id=res["class_id"]
        t_id=""
        t_id = res["track_id"]
        x = res["x"]
        y = res["y"]
        w = res["width"]
        h = res["height"]
        score=""
        # score=res["score"]
        # score=round(score,2)
        txt=str(class_id)+":"+str(t_id)+":"+str(score)

        print(class_id)
        # 在图像上绘制边界框和标识
        cv2.rectangle(frame, (int(x), int(y)), (int(w + x), int(h + y)), (255, 0, 0),2)
        cv2.putText(frame, str(txt), (int(x+w/2), int(y+h/2)), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 0, 0), 8)

        # 判断进出
        start_point = (int(frame.shape[1] * 0.5), int(0))
        end_point = (int(frame.shape[1] * 0.3), int(frame.shape[0]))
        point_0 = (int(x + (w / 2)), int(y + (h / 2)))
        cv2.circle(frame, point_0, 4, (0, 255, 0), 1)
        temp = bool_enter_out((x + (w / 2), y + 10), start_point, end_point)
        count_enter_out(t_id, temp, conf)

# 在图像上绘制直线
def plot_line(img, start_point, end_point):
    cv2.line(img, start_point, end_point, (0, 0, 255), 2)


# 视频捕获线程
def video_capture_thread(cap, video_queue):
    while True:
        # start_t=time.time()
        ret, frame = cap.read()
        # end_t=time.time()
        # dt = (end_t - start_t) * 1000
        # print("视频帧获取耗时: {:.2f} 毫秒".format(dt))
        if not ret:
            print("Failed to retrieve frame")
            break
        video_queue.put(frame)

# 可视化
def visualization_thread(visualization_queue):
    while True:
        frame = visualization_queue.get()
        cv2.imshow('IP Camera', frame)

        # 按下ESC键退出程序
        if cv2.waitKey(1) == 27:
            break

    cv2.destroyAllWindows()

# 处理视频流线程
def process_video(model_url, video_queue, visualization_queue):
    # 配置初始化
    conf = Config()

    # 定义一个函数，用于处理网络请求
    def process_request(img_data, frame):
        try:
            start_t=time.time()
            res = requests.post(model_url, data=img_data.tobytes(), headers={'Content-Type': 'image/jpeg'})
            end_t=time.time()
            dt = (end_t - start_t) * 1000
            print("网络请求耗时: {:.2f} 毫秒".format(dt))   

            res = res.json()

            if res["data"]:
                plot_result(frame, res["data"], conf)
            

             # 画线和标记计数
            start_point = (int(frame.shape[1] * 0.5), int(0))
            end_point = (int(frame.shape[1] * 0.3), int(frame.shape[0]))
            plot_line(frame, start_point, end_point)

            cv2.putText(frame, f'enter: {conf.count_enter}, out: {conf.count_out}', (0, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, (128, 0, 128), 4)

            visualization_queue.put(frame)  # 将帧放入可视化队列

        except requests.RequestException as e:
            print(f"Request to model service failed: {str(e)}")

    while True:
        frame = video_queue.get()  # 从队列获取帧

        # 将图像帧编码为JPEG格式的二进制数据
        _, img_data = cv2.imencode('.jpg', frame)

        size_in_mb = len(img_data) / (1024 * 1024)
        print(size_in_mb, "MB")

        process_request(img_data,frame)
        
def main():

    # 配置参数
    url = r"rtsp://192.168.1.5/102"
    model_url = "http://192.168.1.5:9999/car1" 

    # 视频捕获初始化
    cap = cv2.VideoCapture(url)
    if not cap.isOpened():
        print("Cannot open camera")
        return

    # 创建视频帧队列
    video_queue = queue.Queue(maxsize=10)

    # 创建可视化帧队列
    visualization_queue = queue.Queue(maxsize=10)

    # 创建视频捕获线程
    video_thread = threading.Thread(target=video_capture_thread, args=(cap, video_queue))
    video_thread.daemon = True  # 设置为守护线程，以便在主线程退出时自动结束
    video_thread.start()

    # 创建处理视频流线程
    process_thread=threading.Thread(target=process_video, args=(model_url, video_queue, visualization_queue))
    process_thread.daemon = True
    process_thread.start()

    # 可视化
    cv2.namedWindow('IP Camera', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('IP Camera', 1280, 720)
    visualization_thread(visualization_queue)
    

if __name__ == "__main__":
    main()
