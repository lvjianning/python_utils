import cv2
import requests
import json

# 发送请求并获取响应
url = 'http://192.168.1.6:10000/infer'
image_path = r'H:\360MoveData\Users\Administrator\Desktop\lock_object_detection\lock\3.jpg'
with open(image_path, 'rb') as image_file:
    image_data = image_file.read()

response = requests.post(url, data=image_data)

result = response.json()
print(result)

# 读取原始图像
image = cv2.imread(image_path)
original_height, original_width = image.shape[:2]

# 检查是否成功获取结果
if result['success']:
    # 遍历检测到的对象
    for obj in result['data']:
        # 解析边界框和类别名称
        x1, y1, x2, y2 = obj['x1'], obj['y1'], obj['x2'], obj['y2']
        class_name = obj['class_name']
        
        # 绘制边界框
        start_point = (int(x1), int(y1))
        end_point = (int(x2), int(y2))
        color = (0, 255, 0)  # 绿色
        thickness = 2
        image = cv2.rectangle(image, start_point, end_point, color, thickness)

        # 在边界框上方添加文本
        cv2.putText(image, class_name, (start_point[0], start_point[1] - 10), 
                    cv2.FONT_HERSHEY_SIMPLEX, 1, color, 2)

# 显示结果图像
cv2.namedWindow("Detected Image",cv2.WINDOW_NORMAL)
cv2.resizeWindow("Detected Image",600,800)
cv2.imshow("Detected Image", image)
cv2.waitKey(0)
cv2.destroyAllWindows()
