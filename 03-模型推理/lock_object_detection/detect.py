import argparse
import os
import time
import numpy as np
import cv2
from cvu.detector.yolov5 import Yolov5 as Yolov5Onnx
from vidsz.opencv import Reader, Writer
from cvu.utils.google_utils import gdrive_download

from flask import Flask, request, jsonify

app = Flask(__name__)
class Yolov5OnnxInfer:
    def __init__(self, weight="best.onnx", device="cpu") -> None:
        self.model = Yolov5Onnx(classes=["lock", "unlock","yes"],
                       backend="onnx",
                       weight=weight,
                       device=device)

    def predict(self, image):
        # 保存原始图像尺寸
        original_height, original_width = image.shape[:2]

        # 调整图像尺寸
        resized_image = cv2.resize(image, (640, 640))

        # 执行推理
        preds = self.model(resized_image)

        # 调整边界框尺寸
        scale_width = original_width / 640
        scale_height = original_height / 640

        scaled_preds = []
        for pred in preds:
            bbox = pred._bbox  # 假设这是边界框的列表 [x1, y1, x2, y2]
            bbox_scaled = [bbox[0] * scale_width, bbox[1] * scale_height, 
                           bbox[2] * scale_width, bbox[3] * scale_height]
            scaled_preds.append((pred._class_name, bbox_scaled))

        return scaled_preds


model = Yolov5OnnxInfer()

@app.route("/")
def test():
    return "Hello World"


# REST API的入口，post方式
@app.route("/infer", methods=["POST"])
def predict():
    result = {"success": False}
    if request.method == "POST":
        try:
            # 从请求体中读取二进制数据
            input_image = request.data

            # 将二进制数据转换为图像
            imBytes = np.frombuffer(input_image, np.uint8)
            iImage = cv2.imdecode(imBytes, cv2.IMREAD_COLOR)
            print("image:", iImage.shape)

            # 执行推理并获取调整后的边界框
            scaled_preds = model.predict(iImage)
            for class_name, bbox_scaled in scaled_preds:
                data = {
                    "x1": bbox_scaled[0],
                    "y1": bbox_scaled[1],
                    "x2": bbox_scaled[2],
                    "y2": bbox_scaled[3],
                    "class_name": class_name
                }
                result.setdefault("data", []).append(data)
            result['success'] = True

        except Exception as e:
            result['error'] = str(e)
            print(e)

    return jsonify(result)



if __name__ == "__main__":
    # REST API的IP地址和端口
    app.run(host='0.0.0.0', port=10000)
